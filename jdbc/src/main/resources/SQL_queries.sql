--CREATE
CREATE TABLE invoices (
invoice_id SMALLINT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
client_id SMALLINT(3) UNSIGNED,
invoice_date DATE NOT NULL,
invoice_amount DECIMAL(10,2) UNSIGNED NOT NULL,
invoice_description TINYTEXT,
PRIMARY KEY(invoice_id),
INDEX(invoice_date)
);
CREATE TABLE clients (
client_id SMALLINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
client_name VARCHAR(40) NOT NULL,
client_street VARCHAR(80),
client_city VARCHAR(30),
client_state CHAR(2),
client_zip MEDIUMINT(5) UNSIGNED,
client_phone VARCHAR(14),
contact_name VARCHAR(40),
contact_email VARCHAR(60),
PRIMARY KEY(client_id),
INDEX(client_name)
);
CREATE TABLE expenses (
expense_id SMALLINT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
expense_category_id TINYINT(3) UNSIGNED,
expense_amount DECIMAL(10,2) UNSIGNED,
expense_date DATE,
PRIMARY KEY(expense_id)
);
CREATE TABLE expense_categories (
expense_category_id TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
expense_category VARCHAR(30),
PRIMARY KEY(expense_category_id)
);

--INSERT
INSERT INTO expense_categories 
VALUES (1, 'Books'),
	(2, 'Web Hosting'),
	(3, 'Computer Software');
	
INSERT INTO expenses 
VALUES (1, 3, 2350.00, '2017-09-01'),
	(2, 1, 550.00, '2017-09-01'),
	(3, 2, 300.00, '2017-09-10'),
    (4, 3, 1300.00, '2017-09-10'),
    (5, 3, 920.00, '2017-09-12'),
    (6, 2, 60.00, '2017-09-15'),
    (7, 3, 1560.00, '2017-09-20');
INSERT INTO expenses 
VALUES (8, 3, 2030.0012, '2017-09-20');

--SELECT
SELECT e.expense_amount AS amount, c.expense_category as category_name
FROM test.expenses AS e
	JOIN test.expense_categories AS c
	ON e.expense_category_id = c.expense_category_id AND e.expense_category_id = 3
ORDER BY amount;

SELECT e.expense_id, MAX(e.expense_amount) AS amount, c.expense_category as category_name 
	FROM test.expenses AS e
		JOIN test.expense_categories AS c
		ON e.expense_category_id = c.expense_category_id
    GROUP BY e.expense_category_id;
	
SELECT SUM(e.expense_amount) AS amount, c.expense_category as category_name 
FROM test.expenses AS e
	JOIN test.expense_categories AS c
	ON e.expense_category_id = c.expense_category_id
GROUP BY e.expense_category_id
ORDER BY amount DESC;

SELECT SUM(e.expense_amount) AS amount, c.expense_category as category_name 
	FROM test.expenses AS e
		JOIN test.expense_categories AS c
		ON e.expense_category_id = c.expense_category_id
    GROUP BY e.expense_category_id
    HAVING amount > 500
    ORDER BY amount DESC;
	
SELECT COUNT(DISTINCT(e.expense_category_id)) as categories FROM test.expenses AS e;